import { Component, EventEmitter, Output } from "@angular/core";


@Component({
    selector: 'SearchForm',
    moduleId: module.id,
    templateUrl: './search-form.component.html'
    
})
export class SearchFormComponent {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();

    onButtonTap():void {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2){
            this.search.emit(this.textFieldValue);
        }
    }
}