import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import { RouterExtensions } from "nativescript-angular/router";
import { ToastDuration, Toasty } from "nativescript-toasty";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Color, View } from "tns-core-modules/ui/core/view";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    //providers: [NoticiasService] // indica si el constructor requiere el injectable
})
export class SearchComponent implements OnInit {

    resultados: Array<string>;
    // referencia a la variable layout de html a la variable en ts.
    @ViewChild("layout",{static: true}) layout: ElementRef;
    // se llama al service: noticias
    constructor(
        private noticias: NoticiasService, 
        private store: Store<AppState>
        ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f!= null) {
                    const toast = new Toasty({text: "Sugerimos leer: " + f.titulo});
                    toast.setToastDuration(ToastDuration.SHORT);
                    toast.show();
                }
            })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    

    buscarAhora(s: string) {
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            const toast = new Toasty({text: "Error en la busqueda"});
            toast.setToastDuration(ToastDuration.SHORT);
            toast.show();
        });
    }
}
