import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ListViewEventData } from "nativescript-ui-listview";
import * as app from "tns-core-modules/application";
import { DataService } from "../domain/data.service";

@Component({
    selector: "More",
    templateUrl: "./more.component.html",
    providers: [DataService]
})
export class MoreComponent implements OnInit {
    resultados: Array<string> = [];

    constructor( private nombres: DataService ) { }

    ngOnInit(): void {
        this.nombres.agregar("Item 1");
        this.nombres.agregar("Item 2");
        this.nombres.agregar("Item 3");
        
    }
    

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        const that = new WeakRef(this);
        const data = that.get().nombres;
        //const data = this.nombres.buscar();
        const pullRefresh = args.object;
        setTimeout(function () {
            //this.nombres.agregar("XXXX");
            console.log("ejecutando onPullRefresh");
            data.agregar("Nuevo elem");
            console.log("Mirando data: ", data);
            //console.log("Nombres: ", data);
            //pullRefresh.refreshing = false;
            pullRefresh.notifyPullToRefreshFinished();
        }, 1000);
    }

    

    
    

   
}