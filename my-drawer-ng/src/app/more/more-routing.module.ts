import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MoreComponent } from "./more.component";

const routes: Routes = [
    { path: "" , component: MoreComponent }
];

@NgModule({
    imports: [
        NativeScriptRouterModule.forChild(routes)
    ],
    exports: [ NativeScriptRouterModule]
})
export class MoreRoutingModule { }