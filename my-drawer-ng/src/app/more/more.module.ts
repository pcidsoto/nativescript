import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { MoreRoutingModule } from "./more-routing.module";
import { MoreComponent } from "./more.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        MoreRoutingModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        MoreComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MoreModule { }