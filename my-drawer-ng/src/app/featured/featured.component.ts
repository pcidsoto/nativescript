import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { GestureEventData, GridLayout } from "tns-core-modules";
import * as app from "tns-core-modules/application";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(arg: GestureEventData) {
        console.log("Object that triggered the event: " + arg.object);
        console.log("View that triggered the event: " + arg.view);
        console.log("Event name: " + arg.eventName);

        const grid = <GridLayout>arg.object;
        grid.rotate = 0;
        grid.animate({
            rotate: 360,
            duration: 2000
        })
    }
}
