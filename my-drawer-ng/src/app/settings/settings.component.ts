import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { ToastDuration, ToastPosition, Toasty } from "nativescript-toasty";

import * as appSettings from "tns-core-modules/application-settings";


@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    nombreUsuario: string = "";
    usuarioActual: string = "";
    textFieldValue: string = "";
    constructor() {
        // Use the component constructor to inject providers.
    }

    //funcion auxiliar para dialogs
    doLater(fn) { setTimeout(fn, 1000); }

    ngOnInit(): void {
        // ejemplo de dialogs
        /* 
        this.doLater(() =>
            dialogs.action("Mensaje", "Cancelar!", ["Opcion1", "Opcion2"])
                .then((result) => {
                    console.log("resultado: " + result);
                    if (result === "Opcion1") {
                        this.doLater(() => 
                            dialogs.alert({
                                title: "Titulo 1",
                                message: "mje 1",
                                okButtonText: "btn 1"
                            }).then(() => console.log("Cerrado 1!")));
                    } else if (result === "Opcion2") {
                        this.doLater(() => 
                            dialogs.alert({
                                title: "Titulo 2",
                                message: "mje 2",
                                okButtonText:"btn 2"
                            }).then(() => console.log("Cerrado 2!")));
                    }
                }));
                */

    // Uso de toast
    const toast = new Toasty({text: "Hello World!"});
    toast.setToastDuration(ToastDuration.LONG);
    toast.setToastPosition(ToastPosition.TOP);
    toast.setBackgroundColor("#ff9999")
    toast.show();
    
    this.usuarioActual = appSettings.getString("nombreUsuario");
    
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onTap() {
        console.log(this.textFieldValue);
        this.nombreUsuario = this.textFieldValue;
        appSettings.setString("nombreUsuario", this.textFieldValue);
        const nombreUsuario = appSettings.getString("nombreUsuario");
        console.log("storage " + nombreUsuario);
       //this.user.cambiar(this.textFieldValue);
    }




}
