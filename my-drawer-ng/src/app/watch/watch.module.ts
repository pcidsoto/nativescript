import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { WatchRoutingModule } from "./watch-routing.module";
import { WatchComponent } from "./watch.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        WatchRoutingModule
    ],
    declarations: [
        WatchComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class WatchModule { }