import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { WatchComponent } from "./watch.component";

const routes: Routes = [
    { path: "" , component: WatchComponent }
];

@NgModule({
    imports: [
        NativeScriptRouterModule.forChild(routes)
    ],
    exports: [ NativeScriptRouterModule]
})
export class WatchRoutingModule { }