import { Injectable } from "@angular/core";

// decorador @injectable: es de angular para decir que 
// esta clase inkectada a otros componentes que van a declarar
// su dependencia en esta clase atraves de recibir una variable
// de este tipo en su constructor. 

@Injectable()
export class DataService {

    private noticias: Array<string> = [];

    agregar(s: string) {
        this.noticias.push(s);
    }

    buscar() {
        return this.noticias;
    }

}