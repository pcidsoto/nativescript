import { Injectable } from "@angular/core";
import { getJSON, request } from 'tns-core-modules/http';

// sqlite

const sqlite = require("nativescript-sqlite");

// decorador @injectable: es de angular para decir que 
// esta clase inkectada a otros componentes que van a declarar
// su dependencia en esta clase atraves de recibir una variable
// de este tipo en su constructor. 


@Injectable()
export class NoticiasService {

    api: string = "http://7c4c7a496a9f.ngrok.io";

    constructor() {
        this.getDb((db)=> {
            console.dir(db);
            db.each("select * from logs",
            (err, fila) => console.log("fila: ", fila),
            (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("error en getDb"));
    }

    getDb(fnoK, fnError) {
        //instanciando un db
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir db!", err);
            }else {
                console.log("Esta la db Abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE oK");
                        fnoK(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    agregar (s: string) {
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type":"application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {

        this.getDb((db)=> {
            db.execSQL("insert into logs (texto) values (?)", [s],
                (err, id) => console.log("nuevo id: ", id));
        }, () => console.log("error en getDB"));

        return getJSON(this.api + "/get?q=" + s);
    }

}