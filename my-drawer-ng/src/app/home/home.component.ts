import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { filter } from "rxjs/operators";
import * as app from "tns-core-modules/application";
import { action } from "tns-core-modules/ui/dialogs";
import { ToastDuration, ToastPosition, Toasty } from "nativescript-toasty";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;

    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject services.
    }

    ngOnInit(): void { 

        

    }

    displayActionDialog() {
        // >> action-dialog-code
        let options = {
            title: "Race selection",
            message: "Choose your race",
            cancelButtonText: "Cancel",
            okButtonText: "btn 1",
            actions: ["Option2", "Option1"]
        };

        action(options).then((result) => {
            console.log("Dialog result: ", result);
            if (result === "Option1") {
                // Do action 1
                console.log("Mostrando opcion 1");
                const toast = new Toasty({text: "Hello World!"});
                toast.setToastDuration(ToastDuration.LONG);
                toast.setToastPosition(ToastPosition.CENTER);
                toast.show();
            } else if (result === "Option2") {
                // Do action 2
                console.log("Mostrando Opcion 2 ->");
            }
        });
        // << action-dialog-code
    }

    


    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    //funcion que controla la sidebar
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
